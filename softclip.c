#include <stdlib.h>

#define MIN16(a,b) ((a) < (b) ? (a) : (b))   /**< Minimum 16-bit value.   */
#define MAX16(a,b) ((a) > (b) ? (a) : (b))   /**< Maximum 16-bit value.   */
#define ABS16(x) ((x) < 0 ? (-(x)) : (x))

static void opus_pcm_soft_clip(float * const pcm, const int nb_samples, const int nb_channels, float * const declip_mem)
{
   if (nb_channels<1 || nb_samples<1 || !pcm || !declip_mem) return;

   /* First thing: saturate everything to +/- 2 which is the highest level our
      non-linearity can handle. At the point where the signal reaches +/-2,
      the derivative will be zero anyway, so this doesn't introduce any
      discontinuity in the derivative. */
   for (int i=0;i<nb_samples*nb_channels;i++) {
      pcm[i] = MAX16(-2.f, MIN16(2.f, pcm[i]));
   }

   for (int c=0;c<nb_channels;c++)
   {
      float * const x = pcm+c;
      float a = declip_mem[c];
      /* Continue applying the non-linearity from the previous frame to avoid
         any discontinuity. */
      for (int i=0;i<nb_samples;i++)
      {
         if (x[i*nb_channels]*a>=0)
            break;
         x[i*nb_channels] = x[i*nb_channels]+a*x[i*nb_channels]*x[i*nb_channels];
      }

      int curr=0;
      const float x0 = x[0];
      while(1)
      {
         int start, end;
         float maxval;
         int special=0;
         int peak_pos;
         int i;
         for (i=curr;i<nb_samples;i++)
         {
            if (x[i*nb_channels]>1 || x[i*nb_channels]<-1)
               break;
         }
         if (i==nb_samples)
         {
            a=0;
            break;
         }
         peak_pos = i;
         start=end=i;
         maxval=ABS16(x[i*nb_channels]);
         /* Look for first zero crossing before clipping */
         while (start>0 && x[i*nb_channels]*x[(start-1)*nb_channels]>=0)
            start--;
         /* Look for first zero crossing after clipping */
         while (end<nb_samples && x[i*nb_channels]*x[end*nb_channels]>=0)
         {
            /* Look for other peaks until the next zero-crossing. */
            if (ABS16(x[end*nb_channels])>maxval)
            {
               maxval = ABS16(x[end*nb_channels]);
               peak_pos = end;
            }
            end++;
         }
         /* Detect the special case where we clip before the first zero crossing */
         special = (start==0 && x[i*nb_channels]*x[0]>=0);

         /* Compute a such that maxval + a*maxval^2 = 1 */
         a=(maxval-1)/(maxval*maxval);
         /* Slightly boost "a" by 2^-22. This is just enough to ensure -ffast-math
            does not cause output values larger than +/-1, but small enough not
            to matter even for 24-bit output.  */
         a += a*2.4e-7f;
         if (x[i*nb_channels]>0)
            a = -a;
         /* Apply soft clipping */
         for (i=start;i<end;i++)
            x[i*nb_channels] = x[i*nb_channels]+a*x[i*nb_channels]*x[i*nb_channels];

         if (special && peak_pos>=2)
         {
            /* Add a linear ramp from the first sample to the signal peak.
               This avoids a discontinuity at the beginning of the frame. */
            float offset = x0-x[0];
            const float delta = offset / peak_pos;
            for (i=curr;i<peak_pos;i++)
            {
               offset -= delta;
               x[i*nb_channels] += offset;
               x[i*nb_channels] = MAX16(-1.f, MIN16(1.f, x[i*nb_channels]));
            }
         }
         curr = end;
         if (curr==nb_samples)
            break;
      }
      declip_mem[c] = a;
   }
}

#include <stdio.h>

int main(int argc, char **arv)
{
    float x[512*2];
    FILE *infile = stdin;
    char *input = NULL;
    size_t len = 0;

    for (int i = 0; getline(&input, &len, infile) != EOF; i++) {
        sscanf(input, "%f", x + i);
    }

    free(input);

    float declip_mem[2] = {};

    opus_pcm_soft_clip(x, 512, 2, declip_mem);
    for (int i = 0; i < 512*2; i++) {
        fprintf(stdout, "%f\n", x[i]);
    }
}
