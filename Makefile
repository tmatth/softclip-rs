RS_TARGET=./soft_clip/target/debug/soft_clip
all: softclip softclip_rs

softclip_rs: ${RS_TARGET}

${RS_TARGET}: soft_clip/src/main.rs
	cd soft_clip && cargo build

reference: softclip
	cat og.txt | ./$^

rs: ${RS_TARGET}
	cat og.txt | ./$^

